# Coding Manual

## Server

### Controllers

Every business logic is defined at `modules/` folder.
Controllers are defined by a static class and each handle are defined by a function of it.

```typescript
// modules/person/person.controller.ts
import { ResourceController } from '../../utils/controller';

export class PersonController extends ResourceController {
  // ...more code
  static store(person) {
    // Do the trick!
  }
}    
```

#### Testing
Each controller must to have a test named by `<controller_name>.test.ts` and it must be grouped in a folder.
The main test runner is Jest, and you can run your test scripts using `npm run server:test`
or `npm run server:test --testNamePattern "PersonController"` to run a specific one.

```typescript
// modules/person/person.test.ts
import { PersonController } from './person.controller';

describe('PersonController', () => {
  // Test it here!
});
```

### Routing

The app routes are defined at `routes.ts` file in a `Route` array.
By example, an HTTP route can be defined by:
```typescript
// routes.ts

// ...more code
{ method: 'POST', endpoint: '/person', handler: PersonController.register }
```
This routes will point to `/api/person` path. The `api` prefix is added by default.
To change or remove the prefix, see the `app.ts` module.

### Validating Parameters

#### Declaring
Validations can be defined at `controllers/<controller_name>/<controller_name>.params.ts`.
A param is a class definition used to specify the Schema validation of the param of a controller function.
```typescript
// modules/person/person.params.ts
import { schema } from '../../utils/schematize';

export class PersonParams {
  @schema({
    type: 'string',
    required: true,
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    required: true,
  })
  age: number;
}
```

The `@schema` is a decorator function that creates a schema utilizing the [JSONSchema](https://spacetelescope.github.io/understanding-json-schema/index.html) specification.
Nesting schemas are supported too using the `$ref` option.

#### Applying

Validations can be applied in a controller function using the `@validate` decorator.
This decorator will wrap the target function with a validator that check the param using the schema.
```typescript
// modules/person/person.controller.ts
import { ResourceController } from '../../utils/controller';
import { validate } from '../../utils/schematize';

export class PersonController extends ResourceController {
  // ...more code
  @validate(PersonParams)
  static store(person: PersonParams) {
    // Do the trick!
  }
}
```
If the param is valid, it run the target function normally. If not, it throws an `IllegalArgumentError`
. In a express server, `IllegalArgumentError`s are converted to a `BadRequestError`.

#### Testing a Validation Error
Surrounding the function call with a `try-catch` statement, it possible to extract the error and assert by it properties.
Each item in `error.errors` is a `ValidationError` instance from the [jsonschema](npmjs.com/package/jsonschema) package.

```typescript
// modules/person/person.test.ts
import { PersonController } from './person.controller';
import { IllegalArgumentError } from '../utils/validation'

describe('PersonController', () => {
  test('store: It should not store a person with a invalid age.', () => {
    try {
      PersonController.store({ name: 'Filipe', age: 110 });
    } catch (error: IllegalArgumentError) {
      expect(error.errors[0].property).toBe('instance.age');
    }
  });
});
```

### Creating Middlewares

#### Declaring

```typescript
// middlewares/authentication.ts

import { AuthenticationError } from '../utils/http/errors';
import { Request, Response } from 'express-serve-static-core';

export function isAuthenticated(req: Request, res: Response, next: Function) {
  if (req.headers.authorization === process.env.TOKEN_SECRET) return next();
  return res.status(401).send(new AuthenticationError('Unauthorized API Token.'));
}
```

### Using JsdataService to create Models
Use [JSData](http://api.js-data.io/) to create Models

```typescript
// modules/person.model.ts
import { model, Model } from '../../services/jsdata';
import { schema } from '../../utils/schematize';

@model()
export class Person extends Model<Person> {
  @schema({
    type: 'string'
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    minimum: 18,
  })
  age: number;
}
```
The `Model<T>` has Model functions (as `find`, `findAll`, `update` etc).
To better TypeScript compatibility, the `Model<T>` class requires a Generic class to work with a well-typed functions in
the model functions and validations on the class constructor.
The `@model()` decorator creates the schema in the JSData's `store`
and set up a link between the JSData's `Mapper` functions on the `Model<T>` class.

Models can have relations between an other Model. You must have to specify it in the `@schema`.
For example:
```typescript
import { model, Model } from '../../services/jsdata';
import { schema } from '../../utils/schematize';

@model()
export class Person extends Model<Person> {
  @schema({
    type: 'string'
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    minimum: 18,
  })
  age: number;
  
  @schema({
    $ref: '/Address'
  })
  address: Address;
}
```

#### Creating
Model classes have a default constructor that you pass the data by an object.
```typescript
const person = new Person({ name: 'Filipe', age: 19 });
```

And you can save it using the `save` method.
```typescript
await person.save();
```

#### Query and Modify
You can query by these methods:
* `find(query)` to find one document
* `findAll(query)` to find all documents that match a query
* `update(query)` to update one document
* `updateAll(query)` to update all documents that match a query
* `destroy(query)` to update one documents
* `destroyAll(query)` to update all documents that match a query
The query language is the [same of the JSData](http://api.js-data.io/js-data/latest/Query_.html#toc5__anchor)
