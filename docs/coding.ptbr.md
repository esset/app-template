# Codificando

## Server

### Controllers
Todas as *Controllers* são definidas na pasta `modules/`.
As *Controllers* são declaradas por uma classe estática contendo as funções que lidarão com cada rota.
Deve ser nomeado como `<module_name>.controller.ts`.
```typescript
// modules/person/person.controller.ts
import { ResourceController } from '../../utils/controller';

export class PersonController extends ResourceController {
  // ...mais codigo
  static store(person) {
    // Faça sua mágica aqui
  }
}    
```

#### Testes
Cada *Controller* deve conter um script de teste nomeado por `<module_name>.test.ts`, agrupado na mesma
pasta em que a *Controller*. Para rodar todos os testes, basta executar `npm run server:test`.
Para rodar um *test case* única, execute `npm run server:test --testNamePattern "PersonController"`.

```typescript
// modules/person/person.test.ts
import { PersonController } from './person.controller';

describe('PersonController', () => {
  // Realize os testes aqui!
});
```

Para mais informações sobre testes, consulte a documentação do [Jest](https://jestjs.io/docs/en/getting-started)

### Rotas
As rotas da aplicação são definidas no arquivo `/routes.ts`, contendo um *array*
de `Route`.
Uma rota HTTP pode ser definida, por exemplo:
```typescript
// routes.ts

// ...mais rotas
{ method: 'POST', endpoint: '/person', handler: PersonController.register }
```
Esta rota irá apontar para o caminho `/api/person`. O preixo `api` é adicionado por
padrão. Para mudar isso, veja o arquivo `app.ts`.

### Validando Parâmetros de Rotas

#### Declarando
As validações de parametros rota devem ser definidas em um arquivo `<module_name>.request.ts`, na mesma pasta que a *Controller*.
Uma validação parametro é declarada por uma classe, em que informaçao é descrita em cada atributo.
Utilizando do *decorator* `@schema`, é possivel fazer validações de mais alto nível.

```typescript
// modules/person/person.params.ts
import { schema } from '../../utils/schematize';

export class PersonParams {
  @schema({
    type: 'string',
    required: true,
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    required: true,
  })
  age: number;
}
```

O *decorator* `@schema` utiliza da biblioteca [AJV](https://ajv.js.org/) para realizar validações de JSONSchema.
Relacionar *schemas* é possível utilizando da regra `$ref`, identificando pelo nome do *schema* que se deseja relacionar.

#### Aplicando
Para adicionar validaçoes nas funçoes da *Controller*, basta adicionar o *decorator* `@validate` à elas.
Esse *decorator* irá interceptar a função e realizar as validações necessárias utilizando o *schema* definido.

```typescript
// modules/person/person.controller.ts
import { ResourceController } from '../../utils/controller';
import { validate } from '../../utils/schematize';

export class PersonController extends ResourceController {
  // ...mais código
  @validate(PersonParams)
  static store(person: PersonParams) {
    // Faça a sua mágica aqui!
  }
}
```
Se o parâmetro for valido, a função destino é executada normalmente. Se não, é acionado um `IllegalArgumentError`.
Em uma rota HTTP, os erros `IllegalArgumentError` são convertidos em um `BadRequestError`.

#### Testando erros de validação
Em um script de teste, pode-se adicionar um teste para realizar os testes de validação envolvendo a chamada da função com um `try-catch`.
Isso possibilita extrair os erros acionados pela execuçao e aferir testes. Um `ValidationError` possui um array `errors` contendo as informaçoes sobre a origem do erro.

```typescript
// modules/person/person.test.ts
import { PersonController } from './person.controller';
import { IllegalArgumentError } from '../utils/validation'

describe('PersonController', () => {
  test('store: It should not store a person with a invalid age.', () => {
    try {
      PersonController.store({ name: 'Filipe', age: 110 });
    } catch (error: IllegalArgumentError) {
      expect(error.errors[0].dataPath).toBe('.age');
    }
  });
});
```

### Declarando *middlewares*

#### Declarando

```typescript
// middlewares/authentication.ts

import { AuthenticationError } from '../utils/http/errors';
import { Request, Response } from 'express-serve-static-core';

export function isAuthenticated(req: Request, res: Response, next: Function) {
  // Faça seus testes de autenticação...
  return res.status(401).send(new AuthenticationError('Unauthorized API Token.'));
}
```

### Trabalhando com *Models*
Declarar uma *Model* utiliza do decorator `@model`, e deve ser definida num arquivo de nome `<module_name>.model.ts`

```typescript
// modules/person/person.model.ts
import { model, Model } from '../../services/rethinkdb.ts';
import { schema } from '../../utils/schematize';

@model()
export class Person extends Model<Person> {
  @schema({
    type: 'string'
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    minimum: 18,
  })
  age: number;
}
```
A superclasse `Model<T>` adiciona funções para lidar com as *queries* no banco de dados.
Para uma melhor integraçao com o TypeScript, a classe `Model<T>` requer um tipo para trabalhar melhor com as funçoes da classe.
As validações tambem sao realizadas pelo decorator `@schema`.
Modelos podem ter relaçoes entre elas, basta especificar um `$ref` no *schema* da propriedade que a representa.

Por exemplo:
```typescript
import { model, Model } from '../../services/jsdata';
import { schema } from '../../utils/schematize';

@model()
export class Person extends Model<Person> {
  @schema({
    type: 'string'
  })
  name: string;

  @schema({
    type: 'integer',
    maximum: 100,
    minimum: 18,
  })
  age: number;
  
  @schema({
    $ref: 'Address'
  })
  address: Address;
}
```

#### Criando Documentos e Salvando
Classes Modelo tem um construtor padrão em que se pode passar
os atributos que instanciarao esse Documento.
```typescript
const person = new Person({ name: 'Filipe', age: 19 });
```

E você pode salvar utilizando do método `save`
```typescript
await person.save();
```
