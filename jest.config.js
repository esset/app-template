module.exports = {
  "globals": {
    "ts-jest": {
      "tsConfigFile": "./source/tsconfig.json"
    }
  },
  "transform": {
    "^.+\\.ts?$": "./source/node_modules/ts-jest"
  },
  "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.(js?|ts?)$",
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json"
  ]
};
