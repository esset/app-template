import { WebSocketService } from './services/web-socket';
import { ExpressService } from './services/express';
import { RouterService } from './services/router';

// Polyfill to support `async function*` declarations.
if (!(Symbol as any).asyncIterator) {
  (Symbol as any).asyncIterator = (<any>Symbol).asyncIterator || Symbol.for('Symbol.asyncIterator');
}

/**
 * Represents the Server Application.
 */
export class App {
  /**
   * The config initialized by the `configure` function.
   */
  static config;

  /**
   * Configures the server with it routes.
   * @param config
   */
  static configure(config) {
    App.config = config;

    ExpressService.configure({
      appLivePort: config.appLivePort,
      apiRoute: '/api',
      staticRoute: '/app',
    });

    WebSocketService.configure({
      apiRoute: '/api/ws'
    });

    RouterService.setup(
      require('./routes').default
    );
  }

  /**
   * Start listening the express and the websocket servers.
   */
  static start() {
    ExpressService.listen(App.config.httpPort);
    WebSocketService.listen(App.config.wsPort);
  }
}
