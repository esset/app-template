import { UnauthorizedError } from '../utils/http/errors';
import { Request, Response } from 'express-serve-static-core';

export function isAuthenticated(req: Request, res: Response, next: Function) {
  if (req.headers.authorization === process.env.TOKEN_SECRET) return next();
  return res.status(401).send(new UnauthorizedError('Unauthorized API Token.'));
}
