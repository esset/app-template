import Ajv from 'ajv';

/**
 * The main JSONSchema validator container that contains all the schemas
 * See the `ajv` package documentation for more information.
 */
export const validator: Ajv.Ajv = new Ajv({
  logger: false,
  allErrors: true
} as any);

/**
 * Defines a schematized property using the `@schema decorator`
 * based on the JSONSchema draft-07 specification with some mods in
 * `required`, `unique`, `index` fields as a boolean in the property
 * to a better compatibility with ORMs/ODMs.
 */
export interface Schema {
  /**
   * References to another schema by it name.
   * In Data Modeling, it can refers to a relation.
   */
  $ref?: string;
  multipleOf?: number;
  maximum?: number;
  exclusiveMaximum?: boolean;
  minimum?: number;
  exclusiveMinimum?: boolean;
  maxLength?: number;
  minLength?: number;
  pattern?: string;
  additionalItems?: boolean | Schema;
  items?: Schema | Schema[];
  maxItems?: number;
  minItems?: number;
  uniqueItems?: boolean;
  maxProperties?: number;
  minProperties?: number;
  properties?: { [key: string]: Schema };
  required?: boolean;

  /**
   * In Data Modeling, ensures that it value is unique.
   */
  unique?: boolean;

  /**
   * In Data Modeling, ensure that it field is part of a index.
   */
  index?: boolean;
  additionalProperties?: boolean | Schema;
  'enum'?: any[];
  type?: string | string[];
  format?: string;
  allOf?: Schema[];
  anyOf?: Schema[];
  oneOf?: Schema[];
  not?: Schema;
  [key: string]: Schema | any;
}

function initSchema(target: any) {
  Object.defineProperty(target.constructor, '$schema', {
    enumerable: false,
    value: {
      id: target.constructor.name,
      type: 'object',
      properties: {},
      required: [],
      unique: [],
      index: []
    }
  });
}

/**
 * A configurable property decorator used to define a json schema to the main class.
 * This schema is useful in validations, using the `validation` module, and ORM's such
 * as JSData and Thinkyagain.
 * @param {Schema} config
 * @returns {PropertyDecorator}
 */
export function schema(config: Schema): PropertyDecorator {
  return <PropertyDecorator>
    function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      if (!target.constructor.$schema) initSchema(target);
      if (!target.constructor.$schema.properties[propertyKey])
        target.constructor.$schema.properties[propertyKey] = {};

      const {
        required,
        unique,
        index,
        ...restConfig
      } = config;

      if (required || unique) target.constructor.$schema.required.push(propertyKey);
      if (index || unique) target.constructor.$schema.index.push(propertyKey);
      if (unique) target.constructor.$schema.unique.push(propertyKey);

      target.constructor.$schema.properties[propertyKey] = {
        ...target.constructor.$schema[propertyKey],
        ...restConfig
      };

      if (validator.getSchema(target.constructor.$schema.id))
        validator.removeSchema(target.constructor.$schema.id);
      validator.addSchema(target.constructor.$schema, target.constructor.$schema.id);
    };
}
