// @ts-ignore
import { RethinkDBAdapter } from 'js-data-rethinkdb';

export class RethinkDBAdapterPlus extends RethinkDBAdapter {
  async create(mapper, props, opts) {
    const r = (this as any).r;
    let tableAlreadyExists = true;
    try {
      await r.table(mapper.name);
    } catch (err) {
      tableAlreadyExists = false;
    }
    if (tableAlreadyExists) {
      if (!(mapper as any).schema.isMigrated) {
        for (const field of (mapper as any).schema.index) {
          try {
            await r.table(mapper.name).indexCreate(field);
            await r.table(mapper.name).indexWait(field);
          } catch {}
        }
        (mapper as any).schema.isMigrated = true;
      }

      for (const field of (mapper as any).schema.unique) {
        if (!(await r.table(mapper.name).getAll(props[ field ], { index: field }).isEmpty()))
          throw new Error(`Unique constraint violated on field { ${field}: ${props[ field ]} }`);
      }
    }
    return await super.create(mapper, props, opts);
  }
}
