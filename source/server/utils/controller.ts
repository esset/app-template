/**
 * Used to implement CRUD operations in a Controller.
 */
export interface ResourceController {
  index: Function;
  store: Function;
  show: Function;
  update: Function;
  destroy: Function;
}
