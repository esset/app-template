/**
 * Useful to iterate async or sync generator functions.
 * @param {Generator} iterable The generator that will be iterated.
 * @param {Function} iterator The iteration callback.
 * @param errorHandler The error callback
 * @returns {Promise<any>} A promise that resolves when it done.
 */
export async function iterate(iterable: Generator, iterator: Function, errorHandler?) {
  let streamFrame;
  let frame;

  if (!iterable.next)
    return iterator(iterable);

  do {
    try {
      streamFrame = iterable.next();
      frame = streamFrame;

      if (streamFrame instanceof Promise) {
        frame = await streamFrame;
        iterator(frame.value);
      } else iterator(frame.value);
    } catch (error) {
      if (errorHandler) errorHandler(error);
      else throw errorHandler;
    }

  } while (!frame.done);
}
