import debug from 'debug';
import { yellow, magenta, red, blue } from 'colors';

/**
 * Used to extend the `debug` package with some util methods.
 */
export class Debug {
  scope: string;
  addToLog: Function;

  /**
   * Create a `debug` scoped debug tool.
   * @param {String} scope The scope of the debug's logs.
   */
  constructor(scope) {
    this.scope = scope;
    this.addToLog = debug('app:' + this.scope);
  }

  extend(scope) {
    return new Debug(this.scope + ':' + scope);
  }

  /**
   * Display a default log.
   */
  log(...args) {
    const message = args.shift();
    this.addToLog('[LOG] ' + message, ...args);
  }

  /**
   * Display a info message in blue.
   */
  info(...args) {
    const message = args.shift();
    this.addToLog(blue('[INFO] ') + message, ...args);
  }

  /**
   * Display a error message in red.
   */
  error(...args) {
    const message = args.shift();
    this.addToLog(red('[ERROR] ') + message, ...args);
  }

  /**
   * Display a debug message in magenta.
   */
  debug(...args) {
    const message = args.shift();
    this.addToLog(magenta('[DEBUG] ') + message, ...args);
  }

  /**
   * Display a warn message in yellow.
   */
  warn(...args) {
    const message = args.shift();
    this.addToLog(yellow('[WARN] ') + message, ...args);
  }
}
