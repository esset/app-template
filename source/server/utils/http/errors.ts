import { ValidationError } from 'ajv';

export enum HTTPErrorSignal {
  UNAUTHORIZED = 'UNAUTHORIZED',
  NOT_FOUND = 'NOT_FOUND',
  BAD_REQUEST = 'BAD_REQUEST'
}

/**
 * Used to define HTTP errors.
 */
export class HTTPError extends Error {
  /**
   * The name of this Error.
   */
  name: string;

  /**
   * The readable message that explain this Error.
   */
  message: string;

  /**
   * The HTTP status code.
   */
  statusCode: number;

  /**
   * The constant that represents the origin of this Error.
   */
  signal: HTTPErrorSignal;

  constructor(message: string, statusCode: number, signal: HTTPErrorSignal) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    this.statusCode = statusCode;
    this.signal = signal;
    Error.captureStackTrace(this, this.constructor);
  }
}

export class UnauthorizedError extends HTTPError {
  constructor(message: string) {
    super(message, 401, HTTPErrorSignal.UNAUTHORIZED);
  }
}

export class NotFoundError extends HTTPError {
  constructor(message: string) {
    super(message, 404, HTTPErrorSignal.NOT_FOUND);
  }
}

export class BadRequestError extends HTTPError {
  errors: Error[];
  constructor(message: string, errors?: Error[]) {
    super(message, 400, HTTPErrorSignal.BAD_REQUEST);
    this.errors = errors;
  }
}
