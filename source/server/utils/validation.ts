import { validator } from './schematize';
import { ErrorObject } from 'ajv';
import { pick } from 'lodash';

/**
 * Used to launch a validation error.
 */
export class IllegalArgumentError extends Error {
  errors: ErrorObject[];
  constructor(message, errors?) {
    super(message);
    this.errors = errors;
  }
}

/**
 * Used to validate an argument with a schematized class.
 * @param {Function} SchemaClass A schematized class, defined with the `@schema` decorator.
 * @param arg The argument with properties to test.
 * @returns {Promise<any> | any} The validation result. For async schemas, it returns a Promise.
 */
export function validate(SchemaClass: Function | any, arg: any, onlyDefined?): Promise<any> | any {
  const validatorFn = validator.compile(SchemaClass['$schema']);
  const valid = validatorFn(arg);
  if (valid instanceof Promise)
    return valid
      .then(data => {
        return data;
      })
      .catch(validationError => {
        throw new IllegalArgumentError('Validation error.', validationError.errors);
      });
  else {
    if (!valid) {
      throw new IllegalArgumentError('Validation error.', validatorFn.errors);
    }
    let validProperties = arg;
    if (onlyDefined)
       validProperties = pick(arg, Object.keys(SchemaClass['$schema'].properties));
    return validProperties;
  }
}

/**
 * Create a property decorator that wraps a function execution, validating it params before.
 * @param {Function} SchemaClass The class type expected by the first argument of the function.
 * @param opts Options to apply in the validation. `onlyDefined: true` enables the behavior to result only schema defined properties.
 * @returns {PropertyDecorator} The new property decorator.
 */
export function validation(SchemaClass: Function, opts: {onlyDefined: boolean} = {onlyDefined: false}): PropertyDecorator {
  return <PropertyDecorator>
    function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
      let originalHandler;

      // To replace the function without changes in it name.
      const wrap = {
        [propertyKey]: function (arg) {
          const valid = validate(SchemaClass, arg, opts.onlyDefined);
          if (valid instanceof Promise)
            return valid
              .then(data => {
                return originalHandler(data);
              });
          else return originalHandler(valid);
        }
      };

      if (descriptor) {
        originalHandler = descriptor.value;
        descriptor.value = wrap[propertyKey];
      } else {
        originalHandler = target[propertyKey];
        delete target[propertyKey];
        Object.defineProperty(target, propertyKey, {
          configurable: true,
          writable: true,
          enumerable: true,
          value: wrap[propertyKey]
        });
      }
    };
}
