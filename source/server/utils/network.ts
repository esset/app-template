import { resolve4 } from 'dns';
import { promisify } from 'util';
import * as publicIp from 'public-ip';

/**
 * Get the host public IP through a HTTP API.
 */
export async function getPublicIp(): Promise<string> {
  return await publicIp.v4();
}

/**
 * Resolve a domain to it first IP address.
 */
export async function resolveDomain(domain: string): Promise<string> {
  return (await promisify(resolve4)(domain))[0];
}
