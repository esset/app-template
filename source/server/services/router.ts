import { Debug } from '../utils/debug';
const debug = new Debug('services:router');

/**
 * Define a Route.
 */
export interface Route {
  /**
   * The method that will bootstrap this route.
   */
  method: string;

  /**
   * The specified target endpoint.
   */
  endpoint: string;

  /**
   * The function that will handle this route, receiving it data.
   */
  handler: Function;

  /**
   * A list of functions that wraps the execution of the handler.
   */
  middlewares?: any[];

  /**
   * A topic of a route. Used only for Publisher/Listener routes.
   */
  topic?: string;

  /**
   * The response level. Used only for Publisher/Listener routes.
   */
  access?: 'CHANNEL'|'BROADCAST'|'PRIVATE';
}

/**
 * A function that bootstrap a route by a specific process.
 */
export type RouteBootstrapper = (route: Route) => void;

/**
 * A Router service that manages all the routes of the application.
 */
export class RouterService {
  static methods = {};

  /**
   * Register a new method to the router, that can be specified on `method` field of a Route.
   * @param name The name of this method
   * @param bootstrapper The function that initiate and configure all the routes of it method.
   */
  static addMethod(name: string, bootstrapper: RouteBootstrapper) {
    RouterService.methods[name] = bootstrapper;
  }

  /**
   * Bootstrap a list of routes by it defined methods.
   * @param routes
   */
  static setup(routes: Route[]) {
    routes.forEach(route => {
      // Preventing undefined value
      route.middlewares = !route.middlewares ? [] : route.middlewares;

      debug.log(`Routing ${route.method} ${route.endpoint} --> ${route.handler.name}.`);
      RouterService.methods[route.method](route);
    });
  }
}
