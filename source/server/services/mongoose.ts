import mongoose from 'mongoose';
import { Debug } from '../utils/debug';

const debug = new Debug('services:mongodb');

mongoose.connection.once('open', function() {
  debug.log('Connected!');

  mongoose.connection.on('disconnected', function() {
    debug.error('Client disconnected!'.red);
  });

  mongoose.connection.on('error', function(err) {
    debug.error('Error! %s', err.message);
  });
});

/**
 * A Mongoose service that handle the connection with MongoDB,
 * and support tools like JSONSchema to MongooseSchema conversion for a lean
 * integration with the `@schema` decorator.
 */
export class MongooseService {
  /**
   * Connect the configured Mongoose with a MongoDB Server by it URL.
   * @param {String} mongoUrl The MongoDB URL to connect by.
   * @param {Boolean} enableTryAgain Enable a multiple reconnect if the last fail.
   * @returns {Promise} A promise that resolves when the connection is successful.
   */
  static configure({ url, enableTryAgain, ...options }): Promise<any> {
    debug.info('Connecting with %s', url);

    const connectAgain = (err) => {
      if (err) {
        debug.error('Cant connect! Trying again in 30s...\n%s', err.message);
        setTimeout(() => {
          mongoose.connect(url, {}, enableTryAgain ? connectAgain : null);
        }, 30000);
      }
    };

    return mongoose.connect(url, options)
    // @ts-ignore
      .catch(enableTryAgain ? connectAgain : null);
  }
}
