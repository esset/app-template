import thinkagain from 'thinkagain';
import { Model as ThinkyModel, Document, ThinkyInstance } from 'thinky';
import { pick } from 'lodash';

export interface Term {
  run(connection?: any, options?: any, callback?: (err, res) => void);
  table(name: string, options?: any): Term;
  toStream(connection: any, options: any): NodeJS.ReadableStream;
  dbCreate(db: string): Term;
  dbDrop(db: string): Term;
  dbList(): Term;
  tableCreate(table: string, options: any): Term;
  tableDrop(table: string): Term;
  tableList(): Term;
  indexList(): Term;
  indexCreate(name: string, fn: () => void, options: any): Term;
  indexDrop(name: string): Term;
  indexStatus(): Term;
  indexWait(): Term;
  indexRename(oldName: string, newName: string, options: any): Term;
  changes(options?: any): Term;
  insert(documents: any, options?: any): Term;
  update(newValue: any, options?: any): Term;
  replace(newValue: any, options?: any): Term;
  delete(options: any): Term;
  sync(): Term;
  table(table: string, options: any): Term;
  get(key: string): Term;
  getAll(): Term;
  between(start: any, end: any, options?: any): Term;
  minval(): Term;
  maxval(): Term;
  filter(filter: any, options?: any): Term;
  innerJoin(sequence: any, predicate: any): Term;
  innerJoin(sequence: any, predicate: any): Term;
  eqJoin(rightKey: any, sequence: any, options?: any): Term;
  zip(): Term;
  map(): Term;
  withFields(): Term;
  concatMap(transformation: any): Term;
  orderBy(row: Term): Term;
  desc(field: string): Term;
  asc(field: string): Term;
  skip(value: any): Term;
  limit(value: any): Term;
  slice(start: number, end: number, options?: any): Term;
  nth(element: number): Term;
  sample(size: number): Term;
  pluck(selection: string[]): Term;
  now(): Term;
  then<U>(onFulfill: (value: any) =>
    U | PromiseLike<U>, onReject?: (error: any) => U | PromiseLike<U>, onProgress?: (note: any) => any): Promise<U>;
  row(field: string): Term;
  match(value: string): Term;
  add(value: number): Term;
  getJoin(opts: any): Term;
}

/**
 * A Rethinkdb service that wraps a Thinky connection with a Rethinkdb server.
 */
export class RethinkdbService {
  static db: ThinkyInstance;
  static r: Term;
  static models: { [key: string]: ThinkyModel } = {};
  static configure(opts) {
    RethinkdbService.db = thinkagain(opts);
    RethinkdbService.r = this.db.r;
  }
}

/**
 * Define functions to handle with a Model.
 */
export class Model<T> {
  static $Model: ThinkyModel;
  doc: Document;
  constructor(props: T | any) {
    Object.keys(props).forEach(key => {
      this[key] = props[key];
    });
  }

  /**
   * Add a relation with a Parent Model.
   */
  static belongsTo(OtherModel: any, fieldName: string, leftKey: string, rightKey: string) {
    this.$Model.belongsTo(OtherModel.$Model, fieldName, leftKey, rightKey);
  }

  /**
   * Add a relation with a Child Model.
   */
  static hasOne(OtherModel: any, fieldName: string, leftKey: string, rightKey: string) {
    this.$Model.hasOne(OtherModel.$Model, fieldName, leftKey, rightKey);
  }

  /**
   * Add a relation with a Children Model.
   */
  static hasMany(OtherModel: any, fieldName: string, leftKey: string, rightKey: string) {
    this.$Model.hasMany(OtherModel.$Model, fieldName, leftKey, rightKey);
  }

  /**
   * Add a N-N relation.
   */
  static hasAndBelongsToMany(OtherModel: any, fieldName: string, leftKey: string, rightKey: string) {
    this.$Model.hasAndBelongsToMany(OtherModel.$Model, fieldName, leftKey, rightKey);
  }

  /**
   * Add a pre-event handler.
   */
  static pre(event: 'save' | 'delete' | 'validate' | 'init' | 'retrieve', hook: Function) {
    this.$Model.pre(event, hook);
  }

  /**
   * Add a post-event handler.
   */
  static post(event: 'save' | 'delete' | 'validate' | 'init' | 'retrieve', hook: Function) {
    this.$Model.post(event, hook);
  }

  /**
   * Add a listener to this model.
   */
  static addListener(event: 'ready' | 'retrieved', handler: Function) {
    this.$Model.addListener(event, handler);
  }

  /**
   * Add a listener to the documents from this model.
   */
  static docAddListener(event: 'saving' | 'saved' | 'deleted', handler: Function) {
    this.$Model.docAddListener(event, handler);
  }

  /**
   * Filter all documents and retrieve.
   */
  static filter(opts): Term {
    return this.$Model.filter(opts);
  }

  /**
   * Get the first document that matches a filter and retrieve.
   */
  static get(opts): Term {
    return this.$Model.get(opts);
  }

  /**
   * Count all documents that matches a filter.
   */
  static count(opts?): Term {
    return this.$Model.count(opts);
  }

  /**
   * Retrieve all documents ordered.
   */
  static orderBy(opts): Term {
    return this.$Model.orderBy(opts);
  }

  /**
   * Retrieve all documents.
   */
  static getAll(): Term {
    return this.$Model.getAll();
  }

  /**
   * Run a Rethinkdb statement.
   */
  static run(opts?): Term {
    return this.$Model.run(opts);
  }

  /**
   * Execute a Rethinkdb statement.
   */
  static execute(opts?): Term {
    return this.$Model.execute(opts);
  }

  /**
   * For each document of this model.
   */
  static map(opts): Term {
    return this.$Model.map(opts);
  }

  /**
   * Save this document.
   */
  save(): Promise<T>  {
    return new Promise<T>(() => {});
  }
}

async function checkUnique(tableName, schema: any, props: any) {
  const r = RethinkdbService.db.r;
  let tableAlreadyExists = true;
  try {
    await r.table(tableName);
  } catch (err) {
    tableAlreadyExists = false;
  }
  if (tableAlreadyExists) {
    if (!schema.isMigrated) {
      for (const field of schema.index) {
        try {
          await r.table(tableName).indexCreate(field);
          await r.table(tableName).indexWait(field);
        } catch {}
      }
      schema.isMigrated = true;
    }

    for (const field of schema.unique) {
      if (!(await r.table(tableName).getAll(props[ field ], { index: field }).isEmpty()))
      throw new Error(`Unique constraint violated on field { ${field}: ${props[ field ]} }`);
    }
  }
}

function fillMethods<T>(constructor, $Model): T {
  if (constructor.__proto__.name === 'Model') {
    constructor.$Model = $Model;
    constructor.prototype.save = async function (opts?) {
      await checkUnique(constructor.name, constructor.$schema, this);
      let validProperties = this;
      if (constructor['$model'].onlyDefined)
        validProperties = pick(this, Object.keys(constructor['$schema'].properties));
      return $Model.save(validProperties, opts);
    };
    return constructor;
  } else {
    const modelClass = (class extends Model<T> {
      static $Model: ThinkyModel = $Model;
      doc: Document;
    } as any);
    modelClass.prototype.save = async function (opts?) {
      await checkUnique(constructor.name, constructor.$schema, this);
      let validProperties = this;
      if (constructor['$model'].onlyDefined)
        validProperties = pick(this, Object.keys(constructor['$schema'].properties));
      return $Model.save(validProperties, opts);
    };
    return modelClass;
  }
}

/**
 * A class decorator to instantiate a model class.
 */
export function model(opts: {onlyDefined: boolean} = {onlyDefined: true}): ClassDecorator {
  return function modelDecorator<T>(constructor): T {
    const $Model = RethinkdbService.db.createModel(constructor.name, constructor.$schema);
    constructor['$model'] = opts;
    const newConstructor = fillMethods<T>(constructor, $Model);
    RethinkdbService.models[constructor.name] = $Model;
    buildRefRelations(constructor.name, constructor.$schema, (newConstructor as any).$Model);
    return newConstructor;
  };
}

function buildRefRelations(parentName: string, parentSchema, $Model: ThinkyModel) {
  Object.keys(parentSchema.properties).forEach(key => {
    const isArray = parentSchema.properties[key].type === 'array';
    const refName = isArray ?
      parentSchema.properties[key].items.$ref
      : parentSchema.properties[key].$ref;
    const RefModel = (refName ? RethinkdbService.models[refName] : null) as any;

    if (RefModel) {
      if (isArray) {
        $Model.hasMany(RefModel, key, 'id', parentName.toLowerCase() + 'Id');
      } else {
        $Model.hasOne(RefModel, key, 'id', parentName.toLowerCase() + 'Id');
      }
    }
  });

}
