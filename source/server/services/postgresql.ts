import { Pool } from 'pg';
import { Debug } from '../utils/debug';

const debug = new Debug('services:pg');

/**
 * A Postgresql service that wraps a Pool from the `pg` package
 * and handle the connection with a PG server.
 */
export class PostgresqlService {
  static pool: Pool;

  static configure(postgresURL, maxConnections) {
    debug.log('Connecting', postgresURL);

    PostgresqlService.pool = new Pool({
      connectionString: postgresURL,
      max: maxConnections,
    });

    PostgresqlService.pool.on('error', (err) => {
      debug.error('Unexpected error in PG client.', err);
    });
  }

  static async getClient() {
    return await PostgresqlService.pool.connect();
  }

  static async query(queryString: string, params?: Array<any>): Promise<any> {
    const client = await PostgresqlService.getClient();
    const result = (await client.query.call(client, queryString, params)).rows;
    client.release();

    return result;
  }
}
