import Memcached from 'memcached';
import { promisify } from 'util';

export class MemcachedService {
  static db;

  static get: Function;
  static delete: Function;
  static set: Function;

  static async getJson<T>(key: string): Promise<T> {
    return JSON.parse(await this.get(key));
  }

  static async setJson(key: string, value: any, lifetime: number) {
    return await this.set(key, JSON.stringify(value), lifetime);
  }

  static configure(connectionString) {
    this.db = new Memcached(connectionString);
    this.get = promisify(this.db.get).bind(this.db);
    this.delete = promisify(this.db.del).bind(this.db);
    this.set = promisify(this.db.set).bind(this.db);
  }
}
