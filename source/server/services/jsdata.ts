import { Container, Mapper, Query, Schema } from 'js-data';

export class JsdataService {
  static store = new Container();
}

interface RelationDescription {
  [key: string]: {
    localKeys?: string;
    localKey?: string;
    foreignKeys?: string;
    foreignKey?: string;
    localField: string;
  };
}

export interface ModelOptions {
  adapter?: string;
  relations?: {
    belongsTo?: RelationDescription
    hasMany?: RelationDescription
    hasOne?: RelationDescription
  };
}

export class Model<T> {
  static $mapper: Mapper;
  constructor(props: T | any) {
    Object.keys(props).forEach(key => {
      this[key] = props[key];
    });
  }
  static destroy(id: string|number, opts?: any): Promise<any> {
    return this.$mapper.destroy(id, opts);
  }
  static destroyAll(query: Query, opts?: any): Promise<any> {
    return this.$mapper.destroyAll(query, opts);
  }
  static find<T>(id: string|number, opts?: any): Promise<T> {
    return this.$mapper.find(id, opts);
  }
  static findAll<T>(query: Query, opts?: any): Promise<T[]> {
    return this.$mapper.findAll(query, opts);
  }
  static update(id: string|number, props: any, opts?: any): Promise<any> {
    return this.$mapper.update(id, props, opts);
  }
  static updateAll(props: any, query: Query, opts?: any): Promise<any> {
    return this.$mapper.updateAll(props, query, opts);
  }
  static sum(field: string, query: any, opts?: any): Promise<any> {
    return this.$mapper.sum(field, query, opts);
  }
  static count(query: any, opts?: any): Promise<any> {
    return this.$mapper.count(query, opts);
  }
  save?(opts?): Promise<any> {
    return Model.$mapper.create(this, opts);
  }
}

function fillMethods<T>(constructor, mapper: Mapper): T {
  if (constructor.__proto__.name === 'Model') {
    constructor.$mapper = mapper;
    constructor.prototype.save = function (opts?) { return mapper.create(this, opts); };
    return constructor;
  } else {
    const modelClass = (class extends Model<T> {
      static $mapper: Mapper = mapper;
    } as any);
    modelClass.prototype.save = function (opts?) { return mapper.create(this, opts); };
    return modelClass;
  }
}

function buildRefRelations(parentName: string, parentSchema: Schema, deep?: boolean) {
  const options: ModelOptions = { relations: { hasMany: {}, hasOne: {} } };

  Object.keys(parentSchema.properties).forEach(key => {
    const isArray = parentSchema.properties[key].type === 'array';
    const hasRef = isArray ?
      parentSchema.properties[key].items.$ref
      : parentSchema.properties[key].$ref;
    const refName = hasRef ? hasRef.split('/')[1].toLowerCase() : null;
    const refMapper = (hasRef ? JsdataService.store.getMapperByName(refName) : null) as any;
    if (refMapper) {
      if (isArray) {
        parentSchema.properties[key + 'Id'] = new Schema({
          type: 'array',
          items: { type: 'string' }
        });
        options.relations.hasMany[refName] = {
          localKeys: key + 'Id',
          localField: key
        };

        refMapper.relations = {
          belongsTo: {
            [parentName]: {
              foreignKeys: key + 'Id',
              localField: parentName
            },
            ...refMapper.relations.belongsTo
          },
          ...refMapper.relations
        };

      } else {
        refMapper.schema.properties[parentName + 'Id'] = new Schema({ type: 'string' });

        options.relations.hasOne[refName] = {
          foreignKey: parentName + 'Id',
          localField: key
        };
        refMapper.relations = {
          belongsTo: {
            [parentName]: {
              foreignKey: parentName + 'Id',
              localField: parentName
            },
            ...refMapper.relations.belongsTo
          },
          ...refMapper.relations
        };
      }
      console.log(parentSchema);
      console.log(refMapper.schema);

      if (deep)
        buildRefRelations(parentName, refMapper.schema, true);
    }
  });

  return options;
}

export function model(options?: ModelOptions): ClassDecorator {
  return function modelDecorator<T>(constructor): T {
    const schema = new Schema((constructor as any).$schema);
    const modelName = constructor.name.toLowerCase();
    options = options || buildRefRelations(modelName, schema);
    options.adapter = options.adapter || 'default';
    const mapper = JsdataService.store.defineMapper(modelName, { schema, ...options });
    return fillMethods<T>(constructor, mapper);
  };
}
