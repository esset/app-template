/**
 * The main runnable file that executes the server in the default configuration.
 */

import { App } from './app';
import { config } from './config';

App.configure(config.env);
App.start();
