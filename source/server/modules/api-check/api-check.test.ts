import { ApiCheckController } from './api-check.controller';

describe('ApiCheckController', () => {
  test('status: It should return API OK', () => {
    expect(ApiCheckController.status()).toBe('API OK');
  });
});
