import { agent } from 'supertest';
import * as io from 'socket.io-client';
import * as httpStatus from 'http-status';
import { ExpressService } from '../services/express';
import { WebSocketService } from '../services/web-socket';
import { App } from '../app';
import { config } from '../config';

App.configure(config.testing);
WebSocketService.listen(config.testing.wsPort);

const ioClientOpts = {
  transports: ['websocket'],
  forceNew: true
};

describe('app', () => {
  test('get(/api): It should up the server API.', async () => {
    const response = await agent(ExpressService.server)
      .get('/api');
    expect(response.status).toBe(httpStatus.OK);
  });

  test('ws(/api/ws/): It should up the WebSocket server API.', async () => {
    const message = new Promise(res => {
      io.connect('ws://localhost:' + config.testing.wsPort + '/api/ws/', ioClientOpts)
        .on('api_status', res);
    });

    const response = await message;
    return expect(response).toBe('API OK');
  });
});
