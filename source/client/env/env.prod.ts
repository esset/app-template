export const environment = {
  production: true,
  apiUrl: `http://${location.hostname}:8020/api`,
  wsUrl: `//${location.hostname}:8021/api/ws`,
  apiToken: ''
};
