import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';

import { SocketModule } from './modules/socket';
import { ApiClientModule, ApiClientProvider } from './modules/api-client';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../env/env';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    SocketModule,
    ApiClientModule,

    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor(apiClient: ApiClientProvider) {
    apiClient.configure(environment.apiUrl, environment.apiToken);
  }
}
