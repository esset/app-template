import { NgModule } from '@angular/core';

import { SocketProvider } from './socket.provider';

export { SocketProvider };

@NgModule({
  imports: [],
  providers: [ SocketProvider ],
})
export class SocketModule { }
