import {Observable} from 'rxjs';
import * as io from 'socket.io-client';
import { environment } from '../../../env/env';

if (!environment.production)
  (window as any).io = io;

export class SocketProvider {

  connect(url: string) {
    const socket = io(url);
    const socketDriver = {
      emit: (topic: string, message: any) => {
        socket.emit(topic, message);
        return socketDriver;
      },
      listen: (topic: string) => {
        const observable = new Observable((observer) => {
          const topicHandler = (data) => {
            observer.next(data);
          };
          socket.on(topic, topicHandler);

          return () => {
            socket.off(topic, topicHandler);
            socket.disconnect();
          };
        });
        return observable;
      }
    };

    return socketDriver;
  }
}
