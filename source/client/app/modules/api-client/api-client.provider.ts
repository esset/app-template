import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ApiClientProvider {
  private url: string;

  private defaultOptions: any = {};

  configure(url: string, token: string) {
    this.url = url;
    this.defaultOptions.headers.set('x-access-token', token);
  }

  constructor(private http: HttpClient) {
    const headers = new HttpHeaders();
    this.defaultOptions.headers = headers;
  }

  get(url: string, options?): Observable<any> {
    return this.http.get(
      this.url + '/' + url,
      { ...this.defaultOptions, ...options }
    );
  }

  post(url: string, body: any, options?): Observable<any> {
    return this.http.post(
      this.url + '/' + url,
      body,
      { ...this.defaultOptions, ...options }
    );
  }

  put(endpoint: string, body: any, options?): Observable<any> {
    return this.http.put(
      this.url + '/' + endpoint,
      body,
      { ...this.defaultOptions, ...options }
    );
  }

  delete(endpoint: string, options?): Observable<any> {
    return this.http.delete(
      this.url + '/' + endpoint,
      { ...this.defaultOptions, ...options }
    );
  }

}
