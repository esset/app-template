import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { ApiClientProvider } from './api-client.provider';

export { ApiClientProvider };

@NgModule({
  imports: [ HttpClientModule ],
  providers: [ ApiClientProvider ],
})
export class ApiClientModule { }
