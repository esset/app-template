# App Template
An App starter enabled with the best of TDD in a integration System.

## Backend Support

### Philosophy
* Controllers with a functional paradigm, providing high testability.
* Services with a statical singleton pattern, making the code more readable and concise.

### Roadmap
#### Tooling
* [X] [Jest](https://jestjs.io/) as test runner with supertest for HTTP tests.
* [ ] [Faker](https://github.com/marak/Faker.js/) to generate fake data to use in tests.
* [ ] [Puppeteer](https://pptr.dev/) for Server-Side rendering in webapp projects.
* [X] [Gitlab CI](https://about.gitlab.com/features/gitlab-ci-cd/).
* [X] [Docker](https://www.docker.com/).
* [ ] Documentation with [JSDOC](http://usejsdoc.org/)/[TypeDOC](http://typedoc.org).
* [ ] [Swagger UI](https://swagger.io/tools/swagger-ui/) API explorer generator integration.
* [ ] [Doc genaration](https://github.com/n3ps/json-schema-to-jsdoc) by schemas.
* [X] [Nodemon](https://nodemon.io/) for development livereload.
* [ ] Heroku NPM Deploy Scripts.
* [X] Git Precommit hooks with [Husky](https://www.npmjs.com/package/husky) (Running Lint).
* [ ] [Gulp](https://gulpjs.com/) for CLI's code generator.

#### Code Quality
* [X] [Editorconfig](https://editorconfig.org/)
* [X] TSLint, more flexible, based on Airbnb code style and Angular best practices.

#### CI
* [X] Deploy on Tag.
* [X] Test running.
* [X] Gitlab Container Registry.

#### Library
* [X] Socket.IO for Websockets.
* [X] Express as main HTTP server.
* [ ] [Fastify](https://www.fastify.io/) HTTP server support.
* [ ] Lusca for HTTP security.
* [ ] Helmet for HTTP security.
* [ ] Well-made CORS for API HTTP security.
* [ ] [Reflection](http://blog.wolksoftware.com/decorators-metadata-reflection-in-typescript-from-novice-to-expert-part-4) For less code with metadata types.
* [X] [AJV](https://github.com/epoberezkin/ajv) for fast jsonschema validations.
* [ ] [AJV Custom fields](https://github.com/epoberezkin/ajv/issues/147) to add more features.
* [ ] *OPTIONAL* [Class Validator](https://github.com/typestack/class-validator) Support for validations.
* [ ] *OPTIONAL* [OSOM](https://osom.js.org) schema validator inspired by mongoose syntax.
* [ ] *OPTIONAL* [SJV](https://github.com/hiddentao/sjv) another schema validator.
* [ ] JWT Middleware.
* [ ] Passport integration.
* [ ] Mongoose integration.
* [ ] [JSData](http://api.js-data.io/) for agnostic ODM/ORM support.
* [ ] [TypeORM](http://typeorm.io/#/) for TypeScript ORM support.
* [ ] [Iridium](https://www.npmjs.com/package/iridium) for an alternative ODM for mongodb.
* [ ] [Express Cassandra](https://www.npmjs.com/package/express-cassandra) For cassandra support.
* [ ] [Thinodium](https://www.npmjs.com/package/thinodium) for RethinkDB Support.
* [X] [Thinkagain](https://github.com/mbroadst/thinkagain) for RethinkDB Support.
* [X] Mongoose service.
* [ ] PG service.
* [ ] [CASL](https://github.com/stalniy/casl) for agnostic ACL.
* [ ] RxJS for async operations.
* [ ] RxJS for control middlewares.

## Frontend Support
### Roadmap
#### Tooling
* [ ] E2E with Google's Puppeteer.
* [ ] Gulp for CLI's code generator.
* [ ] Gitlab CI.
* [ ] Documentation with JSDOC/TSDOC.
* [X] Git Precommit hooks with Husky (Running Lint).
* [ ] Browser sync for livereload frontend.
* [ ] Imagemin minifier.
* [ ] CSS/SASS minifier.
* [ ] ES6/TS minifier.
* [ ] [Compodoc](https://github.com/compodoc/compodoc) support.

#### Code Quality
* [X] Editorconfig and a Airbnb based TSLint.
* [ ] CSS/SASS lint and HTML lint with ARIA checks.
* [ ] CanIUse Puppeteers validator, to check supported browsers.

#### Supported CI
* [ ] Frontend Test running.
* [ ] Google Lighthouse's reports at every build.
* [ ] Visual UI Change reports at every build with Applitools support.

#### Angular Mode & Library
* [ ] StencilJS Support.
* [X] Angular with NG CLI.
* [ ] PWA.
* [ ] Ionic Mode.
* [ ] UI Skeleton Components.
* [ ] Localforage for Offline interactivity.
* [ ] LazyLoading.

#### Static Mode
* [ ] Static Sites + SASS Support Mode.

## State of Art
* [Hackathon Starter](https://github.com/sahat/hackathon-starter/blob/master/package.json)
* [TypeScript Node Starter](https://github.com/Microsoft/TypeScript-Node-Starter)
* [Nest](https://docs.nestjs.com/)
* [FoalTS](https://foalts.gitbook.io/docs/)
* [AdonisJS](https://adonisjs.com/)
* [Nodal](http://www.nodaljs.com/)
* [EggJS](https://eggjs.org/en/basics/controller.html)
* [ThinkJS](https://thinkjs.org/en)
* [Marblejs](https://marblejs.gitbook.io/marble/routing)

## MongoDB

This app comes with a MongoDB service.
Just log in the container with `docker exec` and run the `mongo admin -u admin -p root` command to log in Mongo console.

### Creating a new DB and a new User

Following the MongoDB Documentation, [Creating a User](https://docs.mongodb.com/manual/tutorial/create-users/), we will create a new DB and a new User.

```
use appdb
db.createUser(
  {
    user: "dbuser",
    pwd: "dbuser",
    roles: [
       { role: "readWrite", db: "appdb" }
    ]
  }
)
```
